"""Allego file reader.

Copyright (c) 2019 NeuroNexus
Copyright (c) 2023 Hervé Rouault <herve.rouault@univ-amu.fr>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software file and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from __future__ import annotations

import json
import logging
import math
import os
from pathlib import Path
from typing import TYPE_CHECKING

import numpy as np

if TYPE_CHECKING:
    from typing import Any

    from numpy import typing as npt

    Aint = npt.NDArray[np.int_]
    Afloat32 = npt.NDArray[np.float32]
    Afloat64 = npt.NDArray[np.float64]

MAX_FILE_LOAD_SIZE_BYTES = 1e10  # arbitrary data load limit


def read_allego_xdat_metadata(datasource_path: Path) -> dict[str, Any]:
    """Return data source meta-data as a dictionary.

    Parameters
    -----------
    datasource_name : str
        full data source name, including the path & excluding file extensions

    Returns:
    --------
    metadata : dict
        all meta-data for the xdat data source
    """
    fname_metadata = datasource_path.expanduser().resolve().with_suffix(".xdat.json")
    try:
        with fname_metadata.open("rb") as fid:
            metadata: dict[str, Any] = json.load(fid)
    except ValueError:
        logging.exception("Could not load metadata file %s", fname_metadata)
    return metadata


def read_allego_xdat_all_signals(
    datasource_path: Path,
    time_start: float | None = None,
    time_end: float | None = None,
) -> tuple[Afloat32, Aint, Afloat64]:
    """Return data source signal data of all signal types over the requested time range.

    Parameters
    ----------
    datasource_name : str
        full data source name, including the path & excluding file extensions
    time_start : float, optional
        requested starting time in seconds
    time_end : float, optional
        requested ending time in seconds

    Returns:
    -------
    signal_matrix, timestamps, time_samples : tuple
        signal_matrix : numpy array
            signal data with shape MxN, where M is number of signals and N is number of
            samples
        timestamps : numpy array
            timestamp data with shape Nx1
        time_samples : numpy array
            time samples (sec) with shape Nx1

    Notes:
    -----
    An Allego xdat file contains four types of signals referred to as 'pri', 'aux',
    'din', and 'dout'.
    This function returns the signal data from all four of these signal types in
    'signal_matrix'.
    """
    metadata = read_allego_xdat_metadata(datasource_path)
    dsource_name = datasource_path.expanduser().resolve()
    fname_signal_array = (
        dsource_name.parent / (dsource_name.stem + "_data")
    ).with_suffix(".xdat")
    fname_timestamps = (
        dsource_name.parent / (dsource_name.stem + "_timestamp")
    ).with_suffix(".xdat")

    fs = metadata["status"]["samp_freq"]
    time_start = metadata["status"]["t_range"][0] if time_start is None else time_start
    time_end = metadata["status"]["t_range"][1] if time_end is None else time_end

    num_samples = math.floor(time_end * fs) - math.ceil(time_start * fs)

    if (num_samples * metadata["status"]["signals"]["total"] * 4) + (
        num_samples * 8
    ) > MAX_FILE_LOAD_SIZE_BYTES:
        valerr_msg = (
            "Requested too large of a dataset to load. Request a smaller time range"
        )
        raise ValueError(valerr_msg)

    tstamp_offset = (
        math.ceil(time_start * fs) - metadata["status"]["timestamp_range"][0]
    )
    if tstamp_offset < 0:
        tstartrng = metadata["status"]["t_range"][0]
        valerr_msg = (
            f"requested time start must be >= starting time of file ({tstartrng})"
        )
        raise ValueError(valerr_msg)
    if tstamp_offset + num_samples > Path(fname_timestamps).stat().st_size / 8:
        tendrng = metadata["status"]["t_range"][1]
        valerr_msg = f"Requested time end is past the ending time of file ({tendrng})"
        raise ValueError(valerr_msg)

    try:
        with fname_timestamps.open("rb") as fid:
            fid.seek(tstamp_offset * 8, os.SEEK_SET)
            timestamps = np.fromfile(fid, dtype=np.int64, count=num_samples)
    except ValueError as ex:
        logging.exception(
            "Could not load timestamp data from file %s : %s", fname_timestamps, ex
        )

    try:
        with fname_signal_array.open("rb") as fid:
            fid.seek(
                tstamp_offset * metadata["status"]["signals"]["total"] * 4, os.SEEK_SET
            )
            raw_sig_array = np.fromfile(
                fid,
                dtype=np.float32,
                count=num_samples * metadata["status"]["signals"]["total"],
            )
    except ValueError as ex:
        logging.exception(
            "could not load signal array data from file %s : %s", fname_timestamps, ex
        )

    # basic check of data integrity
    num_samples_sig_array = int(
        raw_sig_array.size / metadata["status"]["signals"]["total"]
    )
    if timestamps.shape[0] != num_samples_sig_array:
        rterr_msg = "Inconsistent number of samples between timestamps and signal array"
        raise RuntimeError(rterr_msg)

    return (
        np.reshape(
            raw_sig_array,
            (num_samples_sig_array, metadata["status"]["signals"]["total"]),
        ).T,
        timestamps,
        timestamps / fs,
    )
