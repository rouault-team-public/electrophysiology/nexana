"""NeuroNexus to dat converter."""
from __future__ import annotations

import argparse
import logging
import sys
from pathlib import Path

import h5py
import numpy as np

from nexana import allego_file_reader as allego


def nwb_to_dat(nwb_filepath: Path) -> None:
    """Convert a .nwb dataset to .dat for spyking circus.

    Args:
        nwb_filepath (Path): filepath
    """
    logging.info("Trying to open the dataset in .nwb format.")
    logging.info("Converting %s", nwb_filepath.name)

    with h5py.File(nwb_filepath, "r") as f_in:
        dataset = f_in["acquisition/continuous_signals/data"]
        logging.info("Dataset shape: %s", dataset.shape)
        logging.info("Dataset shape: %s", dataset.dtype)
        logging.info("Converting to raw for spike sorting")

        logging.info("Dataset attributes:")
        for attr, itval in dataset.attrs.items():
            logging.info("%s: %s", attr, itval)

        logging.info(
            "dataset min, max, std: %s %s %s",
            np.min(dataset),
            np.max(dataset),
            np.std(dataset),
        )

        dataset[:].tofile(nwb_filepath.parent / (nwb_filepath.stem + ".dat"))


def xdat_to_dat(xdat_filepath: Path) -> None:
    """Convert a .xdat dataset to .dat for spyking circus.

    Args:
        xdat_filepath (Path): filepath without extension
    """
    logging.info("Trying to open the dataset in .xdat format.")
    logging.info("Converting %s", xdat_filepath.name)

    meta = allego.read_allego_xdat_metadata(xdat_filepath)
    status = meta["status"]
    n_prichans = status["signals"]["pri"]
    n_auxchans = status["signals"]["aux"]
    logging.info("Total number of channels: %d", status["signals"]["total"])
    logging.info("Number of primary (amplified) channels: %d", n_prichans)
    logging.info("Number of auxiliary channels: %d", n_auxchans)
    logging.info("Recording duration: %fs", status["dur"])
    logging.info("Sampling frequency: %fHz", status["samp_freq"])

    all_chans, timest, times = allego.read_allego_xdat_all_signals(xdat_filepath)

    pri_chans = all_chans[:n_prichans]

    logging.info("Writing .dat file")
    pri_chans.T.tofile(f"{xdat_filepath}_spy.dat")


def main() -> int:
    """Start the script."""
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description="Convert a neuronexus dataset to .dat")
    parser.add_argument(
        "file",
        help=""".nwb or xdat dataset.
            In case of an xdat dataset, the path should be given without any extension.
        """,
    )
    args = parser.parse_args()
    in_filepath = Path(args.file)

    if in_filepath.with_suffix(".xdat.json").is_file():
        xdat_to_dat(in_filepath)
    elif in_filepath.suffix == ".nwb":
        nwb_to_dat(in_filepath)
    else:
        logging.error("File not recognized: %s", in_filepath)
        return 1

    return 0


if __name__ == "__main__":
    # execute only if run as a script
    sys.exit(main())
