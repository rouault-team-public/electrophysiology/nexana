# Neuronexus dataset analysis

## Installation

```bash
pip install git+https://gitlab.com/rouault-team-public/electrophysiology/nexana
```

## Generation of .dat files for spyking circus

```
nex_to_dat filepath
```
where `filepath` is the path to a .xdat dataset without any extension.
